<?php

/**
 * Class ModelExtensionModuleEncheres
 * Classe pour travailler avec les tables
 * de base de données du module Encheres
 */
class ModelExtensionModuleEncheres extends Model
{

    /**
     * Méthode enregistre un nouveau encheres
     * @param $post: toutes les valeurs soumises
     */
    public function sauvegarderEncheres($post){
        $idProduit = $this->db->escape($post['id_produit']);
        $prix = $this->db->escape($post['prix']);
        $date = $this->db->escape($post['date']);
        $this->db->query("INSERT INTO " . DB_PREFIX . "tp1_enchere SET idProduit = '"
            . (int)$idProduit . "', `prixDepart` = '" . $prix . "', `dateFin` = '" . $date . "'");
    }

    /**
     * La méthode reçoit de la base de données
     * et retourne une liste de toutes les offres
     * @return mixed
     */
    public function getHistoireEncheres(){
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "tp1_histoire_enchere`");

        $query = $this->db->query("SELECT 
                e.id as produit_id, 
                p.name as produit_nom, 
                CONCAT(c.firstname, ' ', c.lastname) as client_nom, 
                h.prix as prix,
                h.dateProp as dateProp
            FROM `" . DB_PREFIX . "tp1_histoire_enchere` as h
	        JOIN `" . DB_PREFIX . "tp1_enchere` as e ON h.idEnchere =  e.id
	        JOIN `" . DB_PREFIX . "product_description` as p on e.idProduit = p.product_id
	        JOIN `" . DB_PREFIX . "customer` as c on h.idClient = c.customer_id
	        ");

        return $query->rows;
    }

    /**
     *La méthode est démarrée lors de l'installation du module
     * et crée les tables de base de données nécessaires
     */
    public function install() {

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "tp1_enchere` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `idProduit` int(11) NOT NULL,
            `prixDepart` float NOT NULL,
            `prixFin` float,
            `dateFin` datetime NOT NULL,
            `idClient` int(11),
            `nomClient` varchar(100),
            PRIMARY KEY (`id`)
        )ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;");

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "tp1_histoire_enchere` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `idEnchere` int(11) NOT NULL,
          `idClient` int(11) NOT NULL,
          `prix` float NOT NULL,
          `dateProp` datetime NOT NULL,
          PRIMARY KEY (`id`)
       )ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;");
    }

    /**
     *La méthode est démarrée lors de la suppression du module
     * et supprime ses tables de la base de données
     */
    public function uninstall()
    {

        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "tp1_histoire_enchere`");
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "tp1_enchere`");
    }

}