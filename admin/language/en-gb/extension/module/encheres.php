<?php
// Heading
$_['heading_title']    = 'Enchéres';


// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified encheres module!';
$_['text_edit']        = 'Edit encheres Module';

// entree
$_['entree_status']     = 'Status';

//Texts pour Encheres
$_['ajoute_enchere_title'] = 'Ajouter une nouvelle enchère';
$_['list_proposition_title'] = 'Liste des propositions';
$_['encheres_enregistrer'] = 'Enregistrer';
$_['tab_produit_nom'] = 'Nom de Produit';
$_['tab_client_nom'] = 'Nom de client';
$_['tab_date'] = 'Date de proposition';
$_['tab_prix'] = 'Prix';
$_['entree_id_produit']     = 'Id de produit';
$_['entree_prix']     = 'Prix';
$_['entree_date']     = 'Date et heure de fin';
$_['entree_date_desc']     = 'Exemple: 06/14/2018 10:00 PM ';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify encheres module!';