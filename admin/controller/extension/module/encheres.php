<?php

/**
 * Class ControllerExtensionModuleEncheres
 * Class Contrôleur pour le module Enchères
 */
class ControllerExtensionModuleEncheres extends Controller
{

    private $error = array();

    /**
     *La méthode est démarrée pendant l'installation du module
     */
    public function install()
    {
        $this->load->model('extension/module/encheres');

        $this->model_extension_module_encheres->install();
    }

    /**
     *La méthode est appelé lorsque le module est désinstallé
     */
    public function uninstall()
    {
        $this->load->model('extension/module/encheres');

        $this->model_extension_module_encheres->uninstall();
    }

    /**
     *La méthode par défaut du module Enchères
     */
    public function index()
    {
        //Chargement du module linguistique
        $this->load->language('extension/module/encheres');
        //Chargement des modeles  nécessaire
        $this->load->model('extension/module/encheres');
        $this->load->model('setting/setting');

        //Création d'une variable avec une liste d'enchères
        $data['HistoireEncheres'] = $this->model_extension_module_encheres->getHistoireEncheres();

        $this->document->setTitle($this->language->get('heading_title'));



        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            //Traitement des données du formulaire de paramètres du module
            if ($this->request->post["form-type"] == "parameters") {

                $this->model_setting_setting->editSetting('module_encheres', $this->request->post);

                $this->session->data['success'] = $this->language->get('text_success');

                $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
            }

            //Traitement des données de formulaire d'ajout d'un nouveau encheres
            if ($this->request->post["form-type"] == "encheres") {
                $this->model_extension_module_encheres->sauvegarderEncheres($this->request->post);
            }
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/encheres', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['action'] = $this->url->link('extension/module/encheres', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        if (isset($this->request->post['module_encheres_status'])) {
            $data['module_encheres_status'] = $this->request->post['module_encheres_status'];
        } else {
            $data['module_encheres_status'] = $this->config->get('module_encheres_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/encheres', $data));
    }

    /**
     * Méthode de vérification des autorisations
     * pour modifier les paramètres du module
     * @return bool
     */
    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/encheres')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}