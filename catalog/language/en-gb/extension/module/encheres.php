<?php
// Heading
$_['heading_title'] = 'Les enchéres';

// Text
$_['text_contact']  = 'Contact Us';
$_['text_sitemap']  = 'Site Map';
$_['text_idProduit']  = 'Id de produit';
$_['text_nomProduit']  = 'Nom de produit';
$_['text_prixDepart']  = 'Dernier prix';
$_['text_separator'] = '&gt;';
$_['text_title'] = 'Title';
$_['text_dateFin'] = 'dernière date de soumission';
$_['text_bonjour'] = 'Bonjour';
$_['text_titre_modal'] = 'Combien vous voulez payer?';
$_['text_fermer_modal'] = 'Fermer';
$_['text_submit_modal'] = 'Soumettre';
$_['text_label_modal'] = 'Entrez votre enchère';
$_['text_votre_enchere'] = 'Votre enchère';
$_['text_connecter_vous'] = 'vous devez vous connecter pour participer';