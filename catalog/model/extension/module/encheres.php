<?php

/**
 * Class ModelExtensionModuleEncheres
 */
class ModelExtensionModuleEncheres extends Model {
    /**
     * La méthode renvoie toutes les marchandises aux participants à l'enchère
     * @return mixed
     */
    public function getToutEncheres(){
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "tp1_enchere` WHERE dateFin > NOW()");
        return $query->rows;
    }

    /**
     * La méthode ajoute l'enchère du client à la base de données
     * @param $post : toutes les valeurs soumises
     */
    public function proposePrix($post){
        $idEnchere = $this->db->escape($post['encheresId']);
        $prix = $this->db->escape($post['offre']);
        $idClient = $this->customer->getId();
        $nomClient= $this->customer->getFirstName()." ".$this->customer->getLastName();
        //mettre à jour la table tp1_enchere avec l'identifiant et le nom du client
        $this->db->query("UPDATE " . DB_PREFIX . "tp1_enchere SET prixFin = '"
            . (float)$prix . "', `idClient` = '" .(int)$idClient . "', `nomClient` = '" .$nomClient . "' WHERE `id`=" . $idEnchere);
        // insérer la nouvelle proposition de prix dans la table tp1_histoire_enchere
        $this->db->query("INSERT INTO " . DB_PREFIX . "tp1_histoire_enchere SET idEnchere = '"
            . (int)$idEnchere . "', `idClient` = '" . (int)$idClient . "', `prix`=" . $prix .", `dateProp` = now() " );
    }
}
