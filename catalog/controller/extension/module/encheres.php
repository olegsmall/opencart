<?php

/**
 * Class ControllerExtensionModuleEncheres
 * Class Contrôleur pour le module Enchères
 */
class ControllerExtensionModuleEncheres extends Controller
{
    private $error = array();

    /**
     *La méthode par défaut du module Enchères
     */
    public function index()
    {
        //Chargement du module linguistique
        $this->load->language('extension/module/encheres');
        //Chargement des modeles  nécessaire
        $this->load->model('extension/module/encheres');
        $this->load->model('catalog/product');

        //Vérifiez si l'utilisateur s'est connecté
        if ($this->customer->isLogged()) {
            $data['clientId'] = $this->customer->getId();
            $data['clientPrenom'] = $this->customer->getFirstName();
            $data['clientNom'] = $this->customer->getLastName();

            //Enregistrement du encheres du client dans la base de données
            if (isset($this->request->post['form-nouveau-enchere'])) {
                $this->model_extension_module_encheres->proposePrix($this->request->post);
            }
        } else {
            $data['clientId'] = '';
        }

        // Titre de la page.
        $this->document->setTitle($this->language->get('heading_title'));
        // Fil d’Ariane
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/encheres'),
            'separator' => $this->language->get('text_separator')
        );

        // Texte pris dans le fichier du langage
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_idProduit'] = $this->language->get('text_idProduit');
        $data['text_nomProduit'] = $this->language->get('text_nomProduit');
        $data['text_prixDepart'] = $this->language->get('text_prixDepart');
        $data['text_ancheres'] = $this->language->get('text_ancheres');

        // Appel de la fonction getToutEncheres du modèle
        $toutEncheres = $this->model_extension_module_encheres->getToutEncheres();
        foreach ($toutEncheres as $enchere) {
            $enchere['nomProduit'] = $this->model_catalog_product->getProduct($enchere['idProduit'])['name'];
            //montrer le prix de début s'il n'y a aucune offre
            $enchere['prix'] = (isset($enchere['prixFin']) ? $enchere['prixFin'] : $enchere['prixDepart']);
            $data['toutEncheres'][] = $enchere;
        }

        // Requis. Les fichiers contenus dans la page.
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller
        ('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        // Appel de la vue
        $this->response->setOutput($this->load->view('extension/module/encheres', $data));
    }

}